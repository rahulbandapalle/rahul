package Abstractiobank;

import java.util.Scanner;

public class MainAPP {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Account Type");
        System.out.println("1:SAVING\n2:LOAN");
        int accType= sc1.nextInt();
        System.out.println("Enter Account Opening Balance");
        double balance=sc1.nextDouble();
        AccountFactory factory=new AccountFactory();

        Account accRef=factory.createdAcoount(accType,balance);
        boolean status=true;
        while(status){
            System.out.println("Select mode of Transcation");
            System.out.println("1:Deposit\n2:withdraw\3:CheckBalance\4:Exit");
            int choice=sc1.nextInt();
            if (choice==1){
                System.out.println("Enter Amount");
                double amt= sc1.nextDouble();
                accRef.deposit(amt);
            }
            else if (choice==2){
                System.out.println("Enter Ammount");
                double amt= sc1.nextDouble();
                accRef.withdraw(amt);
            } else if (choice==3) {
                accRef.checkBalance();

            }else{
                status=false;
            }

        }
    }
}
