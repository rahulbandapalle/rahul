package Abstractiobank;

public class LoanAccount implements Account {
    double LoanAccount;
    public LoanAccount(double LoanAccount){
        this.LoanAccount=LoanAccount;
        System.out.println("Loan Account Created");
    }

    @Override
    public void deposit(double amt) {
        LoanAccount-=amt;
        System.out.println(amt+"Rs Debited From Loan Account");

    }

    @Override
    public void withdraw(double amt) {
        LoanAccount+=amt;
        System.out.println(amt+"Rs Creadit To Your Account");

    }

    @Override
    public void checkBalance() {
        System.out.println("Active Loan Amount"+LoanAccount);

    }
}
