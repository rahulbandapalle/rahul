package Abstractiobank;

public class savingAccount implements Account {
    double accountBalance;
    public savingAccount(double accountBalance){
        this.accountBalance=accountBalance;
        System.out.println("Saving Account Created");
    }
    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+"Rs Credit To Your Account");

    }

    @Override
    public void withdraw(double amt) {
        if(amt<=accountBalance){
            System.out.println("Insufficent+Account Balance");
        }

    }

    @Override
    public void checkBalance() {
        System.out.println("Ative Balance "+accountBalance);

    }
}
