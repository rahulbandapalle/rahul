package program;

public class P22 {
    public static void main(String[] args) {
        int a = 155;
        int sum = 0;
        while (a != 0) {
            int r = a % 10;
            sum += (r * r * r);
            a = a / 10;
        }
        if (sum == 155) {
            System.out.println("AnStrong Number");
        } else {
            System.out.println("Invalid Number");
        }
    }
}
