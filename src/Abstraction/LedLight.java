package Abstraction;

public class LedLight implements Switch {
    @Override
    public void Switchon() {
        System.out.println("Led light switch on");

    }

    @Override
    public void Switchoff() {
        System.out.println("Led light switch off");

    }
}
