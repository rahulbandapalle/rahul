package Abstraction;

class CflLight implements Switch {
    @Override
    public void Switchon() {
        System.out.println("cfl light switch on");
    }

    @Override
    public void Switchoff() {
        System.out.println("cfl light switch off");

    }
}
