package methodoverloading;

public class Mainapp2 {
    // standard main method
    public static void main(String[] args) {
        System.out.println("Main Method 1");
        main(25);
        main('J');


    }

    public static void main(int a) {
        System.out.println("A:"+a);
    System.out.println("Main Method 2");
    }

    public static void main(char c) {
        System.out.println("c:"+c);
        System.out.println("Main Method 3");

    }
}
