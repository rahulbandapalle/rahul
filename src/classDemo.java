class stringDemo{
    public static void main(String [] main)
    {
        String s="Software Developer";
        System.out.println(s.length());
        System.out.println(s.charAt(12));
        System.out.println(s.indexOf('w'));
        System.out.println(s.lastIndexOf('e'));
        System.out.println(s.contains("even"));
        System.out.println(s.startsWith("Soft"));
        System.out.println(s.endsWith("Soft"));
        System.out.println(s.substring(9));
        System.out.println(s.substring(0,8));
        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());
    }
}