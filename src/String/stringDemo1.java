package String;

import javax.xml.soap.SOAPPart;

public class stringDemo1 {
     public static void main(String [] args){
         // constant pool area
         String s1="PUNE";
         String s2="Mumbai";
         String s3="PUNE";

         // non constant area

         String str1=new String("mumbai");
         String str2=new String("PUNE");
         String str3=new String ("PUNE");

         System.out.println(s1==s2); // f
         System.out.println(s1==s3); //t
         System.out.println(s1.equals(str2));
         System.out.println(str2==str3);
         System.out.println(s2.equals(str1));
         System.out.println(s2.equalsIgnoreCase(str1));

     }
}
